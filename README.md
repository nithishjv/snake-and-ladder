# Snake and Ladder

[Problem statement](./Problem_Statement.txt)

## Setup

1. Make sure the local environment has one of the recent versions of [Node.js](https://nodejs.org/en/download/) and `npm` installed.
2. Clone the repo and navigate to the project directory.
3. To install `npm` dependencies,

```bash
npm install
```

4. To run unit tests,

```bash
npm test
```

5. To start the game,

```bash
npm start
```
