export enum Dice {
    Default,
    Crooked
}

export type PlayerPositionMap = {
    [name: string]: number
}

export type PlayerTransitionMap = {
    [start: number]: number;
}

export type GameConfig = {
    snakes: number[][];
}