import Prompt from "prompt-sync";
import Board from "./Board";
import { Dice } from "./types";

const prompt = Prompt();

export function main() {
    console.log("Snake & Ladder!");

    const playerName = prompt("Starting new game.\nEnter player name: ");
    const dice = parseInt(prompt(`Hey ${playerName}! Which dice would you prefer?\nPress 0 for Normal dice\nPress 1 for Crooked dice\n`)) as Dice;

    if (!playerName || !dice) {
        console.error("Invalid inputs. <%j>", { playerName, dice });
        return
    }

    console.log(`Nice! Let's kick it off now with ${Dice[dice]} dice`);


    const board = new Board(dice);
    board.addPlayer(playerName);

    while (Object.values(board.playersPositionsMap).find((position) => position !== Board.BOARD_SIZE)) {
        prompt("Enter to roll dice")
        board.play(playerName)
    }
}

main()