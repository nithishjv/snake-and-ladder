import { main } from ".";

describe("Tests to call main", () => {
    it("should return 'Hello world' while calling main method", () => {
        
        expect(main()).toStrictEqual("Hello world");
    });
});