import Board from "./Board";
import { Dice } from "./types";
// Create basic board and game play skelton for a Single Player.

// 1. The board will have 100 cells.
// 2. The game will have a 6 sided dice numbered from 1 to 6 and will always give a random number on rolling it.
// 3. The player initially starts from outside the board. Move the player to position 1 when the dice value is 6 only.
// 4. For a dice throw, a player should move from the initial position by the number on dice throw. If the dice value is 2 and the piece is at position 2, the player will put their piece at position 4 now (2+2).
// 5. A player wins if it exactly reaches the position 100 and the game ends there.
// 6. Log each step of the game play: such as the current value on the dice, and the players position, status of the game when it ends etc.

describe("Unit tests for Board", () => {
  const dummyPlayer = 'john';

  let board: Board;
  let rollDiceSpy: any;
  function setupBoard() {
    jest.resetAllMocks();

    board = new Board();
    board.addPlayer(dummyPlayer);
    rollDiceSpy = jest.spyOn(board, 'rollDice')
  }
  beforeEach(setupBoard);

  describe("Testing dice behaviour in board", () => {
    it("should pick a random face (1-6) from dice when rolled by default", () => {
      const diceValue = board.rollDice();

      expect(diceValue).toBeLessThan(7);
    });
  });

  describe("Testing playing the game", () => {
    it("should iniate player's position to -1", () => {
      expect(board.playersPositionsMap[dummyPlayer]).toStrictEqual(-1);
    });

    it("should declare player is victorious, if player's position reaches board's size", () => {
      const consoleSpy = jest.spyOn(console, 'log');

      rollDiceSpy.mockReturnValueOnce(0);
      board.playersPositionsMap[dummyPlayer] = Board.BOARD_SIZE;
      board.play(dummyPlayer);

      expect(consoleSpy).toHaveBeenCalledWith('Yayy! <%s> won by rolling a <%d>', dummyPlayer, 0);
      consoleSpy.mockReset()
    });

    it("should start game for a player after rolling a 6", () => {
      rollDiceSpy.mockReturnValueOnce(1);
      board.play(dummyPlayer);
      expect(board.playersPositionsMap[dummyPlayer]).toStrictEqual(-1);

      rollDiceSpy.mockReturnValueOnce(6);
      board.play(dummyPlayer);
      expect(board.playersPositionsMap[dummyPlayer]).toStrictEqual(1);
    });

    it("should move the player's position per dice's face value", () => {
      board.playersPositionsMap[dummyPlayer] = 1;
      rollDiceSpy.mockReturnValueOnce(6);
      board.play(dummyPlayer);

      expect(board.playersPositionsMap[dummyPlayer]).toStrictEqual(7);
    });
  });

  // Add snakes on the board

  // 1. Add some snakes on the board. You are free to choose the postions and the number of snakes.
  // 2. A snake moves a player from its start position to end position. where start position > end position. Eg. A snake starting at 14 and ensing at 7, moves the player from position 14 to position 7.

  describe("Testing snakes on the board", () => {
    it("should load snakes from config while creating the board", () => {
      expect(board.playersTransitionMap).toStrictEqual({ 40: 2, 35: 28, 50: 10 });
    });

    it("should throw error when start position of snake is less than it's end", () => {
      expect(() => {
        const badBoard = new Board(Dice.Default, { snakes: [[30, 40]] })
      }).toThrowError(RangeError);
    });

    it("should throw error when start position of snake is greater than board size", () => {
      expect(() => {
        const badBoard = new Board(Dice.Default, { snakes: [[130, 40]] })
      }).toThrowError(RangeError);
    });

    it("should move user's position to end of snake when encountered", () => {
      board.playersPositionsMap[dummyPlayer] = 39;
      rollDiceSpy.mockReturnValueOnce(1);
      board.play(dummyPlayer);
      expect(board.playersPositionsMap[dummyPlayer]).toStrictEqual(2);
    });
  });

  // Make A Crooked Dice.

  // 1. A Crooked dice is a dice that only throws Even numbers.
  // 2. The game can be started with a normal dice or a crooked dice.

  describe("Testing crooked dice on the board", () => {
    it("should create board with crooked dice and move dice accordingly", () => {
      const board = new Board(Dice.Crooked);
      board.playersPositionsMap[dummyPlayer] = 1;
      board.play(dummyPlayer);
      expect((board.playersPositionsMap[dummyPlayer] - 1) % 2 === 0).toBeTruthy()
    })
  });


});