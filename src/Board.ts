import DefaultGameConfig from "./game-config.json";
import { Dice, GameConfig, PlayerPositionMap, PlayerTransitionMap } from "./types";

const DiceFaces: { [key in Dice]: number[] } = {
  [Dice.Default]: [...Array(6).keys()].map(i => i + 1),
  [Dice.Crooked]: [...Array(6).keys()].map(i => i + 1).filter((i) => i % 2 === 0),
};

function convertCoordinatesToMap(coordinates: any[]): PlayerTransitionMap {
  const directedMap: PlayerTransitionMap = {};
  for (let coordinate of coordinates) {
    const [start, end] = coordinate;
    if (start > Board.BOARD_SIZE) throw RangeError(`Start cannot exceed board size. <${coordinate}>`);
    if (end > start) throw RangeError(`Start cannot exceed end. <${coordinate}>`);
    directedMap[start] = end;
  }
  return directedMap;
}

export default class Board {
  static BOARD_SIZE = 100;
  private readonly diceFaces: number[];
  private readonly dice: Dice;
  playersPositionsMap: PlayerPositionMap = {};
  playersTransitionMap: PlayerTransitionMap = {};

  constructor(dice: Dice = Dice.Default, gameConfig: GameConfig = DefaultGameConfig) {
    this.dice = dice;
    this.diceFaces = DiceFaces[dice];
    this.playersTransitionMap = convertCoordinatesToMap(gameConfig.snakes);
  }

  rollDice(): number {
    const shuffledFace = Math.floor((Math.random() * this.diceFaces.length));
    console.log("Rolled dice and got <%d>", this.diceFaces[shuffledFace]);
    return this.diceFaces[shuffledFace];
  }

  addPlayer(name: string): void {
    this.playersPositionsMap[name] = -1;
  }

  play(name: string): void {
    const diceValue = this.rollDice();
    const currentPosition = this.playersPositionsMap[name];
    let newPosition = currentPosition + diceValue;

    if (newPosition === Board.BOARD_SIZE) {
      this.playersPositionsMap[name] = newPosition;
      console.log("Yayy! <%s> won by rolling a <%d>", name, diceValue);
    } else if (currentPosition < 1 && diceValue === this.diceFaces[this.diceFaces.length - 1]) {
      console.log("Cheers! <%s> is starting the game!", name);
      this.playersPositionsMap[name] = this.dice === Dice.Default ? 1 : 2;
    } else if (currentPosition > 0 && newPosition < Board.BOARD_SIZE) {
      this.playersPositionsMap[name] = this.playersTransitionMap[newPosition] || newPosition;
    } else {
      console.log("Skipping turn. <%s> cannot move further from <%d>", name, this.playersPositionsMap[name]);
    }

    console.log("<%s> is now at <%d> after rolling a <%d>", name, this.playersPositionsMap[name], diceValue);
  }
}